
# 简介 | Intro

该项目用于订餐业务开源项目，只用于学习研究使用。

# 技术框架 | Framework

- 核心框架：Spring Framework 4.3.4.RELEASE
- 视图框架：Spring MVC
- 持久层框架：MyBatis 3.4.1 + Mybatis-Plus 1.4.9
- JS框架：jQuery
- 富文本：kindeditor

# 开发环境 | Env & IDE

建议开发者使用以下环境，这样避免版本带来的问题

- Eclipse: Mars.1 Release (4.5.1) or newer
- MySql: 5.5
- JDK: 8
- tomcat: 8.0.30

# 开发规范 | Development Specification

## git分支管理

参考规范
- 建议开发者都基于`develop`分支进行开发和提交
- 由管理员定期合并并保证`master`分支的可用性

## 其他

待补充

# 已实现功能 | Feature

目前已实现后台的权限和网站配置管理

	演示站:http://127.0.0.1:8080/dining/admin
	账号:admin
	密码:111111

 _后期会把后台调整为bootstrap3样式_ ，有些需要改正的，请大家给我提出建议。

Api文档生成已加入，访问方式: http://127.0.0.1/dining/swagger-ui.html

