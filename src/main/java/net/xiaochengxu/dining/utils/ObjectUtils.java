package net.xiaochengxu.dining.utils;

import java.io.*;
import java.text.DecimalFormat;
import java.text.Format;
import java.util.*;

public class ObjectUtils {

	private static Format FORMAT = new DecimalFormat("#.##");

	public ObjectUtils() {
	}

	public static boolean isNull(Object o) {
		return null == o || "".equals(o);
	}

	public static boolean isNull(List list) {
		return null == list || list.size() == 0;
	}

	public static boolean isNull(Set set) {
		return null == set || set.size() == 0;
	}

	public static boolean isNull(Map map) {
		return null == map || map.size() == 0;
	}

	public static boolean isNull(Long lg) {
		return null == lg || lg.longValue() == 0L;
	}

	public static boolean isNull(Integer it) {
		return null == it || it.intValue() == 0;
	}

	public static boolean isNull(File file) {
		return null == file || !file.exists();
	}

	public static boolean isNull(Object strs[]) {
		return null == strs || strs.length == 0;
	}

	public static Number getNumber(Number number) {
		return ((Number) (isNull(number) ? Long.valueOf(0L) : number));
	}

	public static String numberFormat(Number number, String pattern[]) {
		return isNull((Object[]) pattern) ? FORMAT.format(number) : FORMAT.format(pattern[0]);
	}

	public static boolean isNotNull(Object o) {
		return !isNull(o);
	}

	public static boolean isNotNull(List list) {
		return !isNull(list);
	}

	public static boolean isNotNull(Set set) {
		return !isNull(set);
	}

	public static boolean isNotNull(Map map) {
		return !isNull(map);
	}

	public static boolean isNotNull(Long lg) {
		return !isNull(lg);
	}

	public static boolean isNotNull(Integer it) {
		return !isNull(it);
	}

	public static boolean isNotNull(File file) {
		return !isNull(file);
	}

	public static boolean isNotNull(Object strs[]) {
		return !isNull((Object[]) strs);
	}

}
