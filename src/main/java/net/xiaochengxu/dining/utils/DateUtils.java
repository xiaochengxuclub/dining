package net.xiaochengxu.dining.utils;

import java.sql.Timestamp;
import java.text.*;
import java.util.*;

public class DateUtils {

	public DateUtils() {
	}

	public static String getNowTime() {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		return sdf.format(Calendar.getInstance().getTime());
	}

	public static String getTimeBeforeORAfter(int days) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Calendar c = Calendar.getInstance();
		c.add(5, days);
		return sdf.format(c.getTime());
	}

	public static Date toDate(String date, String pattern) {
		if ((new StringBuilder()).append("").append(date).toString().equals(""))
			return null;
		if (pattern == null)
			pattern = "yyyy-MM-dd";
		SimpleDateFormat sdf = new SimpleDateFormat(pattern);
		Date newDate = new Date();
		try {
			newDate = sdf.parse(date);
		} catch (Exception var5) {
			var5.printStackTrace();
		}
		return newDate;
	}

	public static String toString(Date date, String pattern) {
		if (date == null)
			return "";
		if (pattern == null)
			pattern = "yyyy-MM-dd";
		String dateString = "";
		SimpleDateFormat sdf = new SimpleDateFormat(pattern);
		try {
			dateString = sdf.format(date);
		} catch (Exception var5) {
			var5.printStackTrace();
		}
		return dateString;
	}

	public static Date getNowDate() {
		Date currentTime = new Date();
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String dateString = formatter.format(currentTime);
		ParsePosition pos = new ParsePosition(8);
		Date currentTime_2 = formatter.parse(dateString, pos);
		return currentTime_2;
	}

	public static Date getNowDateShort() {
		Date currentTime = new Date();
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		String dateString = formatter.format(currentTime);
		ParsePosition pos = new ParsePosition(8);
		Date currentTime_2 = formatter.parse(dateString, pos);
		return currentTime_2;
	}

	public static String getStringDate() {
		Date currentTime = new Date();
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String dateString = formatter.format(currentTime);
		return dateString;
	}

	public static String getStringDateShort() {
		Date currentTime = new Date();
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		String dateString = formatter.format(currentTime);
		return dateString;
	}

	public static String getTimeShort() {
		SimpleDateFormat formatter = new SimpleDateFormat("HH:mm:ss");
		Date currentTime = new Date();
		String dateString = formatter.format(currentTime);
		return dateString;
	}

	public static Date strToDateLong(String strDate) {
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		ParsePosition pos = new ParsePosition(0);
		Date strtodate = formatter.parse(strDate, pos);
		return strtodate;
	}

	public static String dateToStrLong(Date dateDate) {
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String dateString = formatter.format(dateDate);
		return dateString;
	}

	public static String dateToStr(Date dateDate) {
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		String dateString = formatter.format(dateDate);
		return dateString;
	}

	public static String dateToStr(Date dateDate, String pattern) {
		SimpleDateFormat sdf = new SimpleDateFormat(pattern);
		return sdf.format(dateDate);
	}

	public static Date strToDate(String strDate) {
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		ParsePosition pos = new ParsePosition(0);
		Date strtodate = formatter.parse(strDate, pos);
		return strtodate;
	}

	public static Date strToDate(String strDate, String patten) {
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		if (patten != null && patten.length() > 0)
			formatter = new SimpleDateFormat(patten);
		ParsePosition pos = new ParsePosition(0);
		Date strtodate = formatter.parse(strDate, pos);
		return strtodate;
	}

	public static Date getNow() {
		Date currentTime = new Date();
		return currentTime;
	}

	public static String getCurrentDateStr() {
		Date currentTime = new Date();
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String dateString = formatter.format(currentTime);
		return dateString;
	}

	public static Date getLastDate(long day) {
		Date date = new Date();
		long date_3_hm = date.getTime() - 0x74bad00L * day;
		Date date_3_hm_date = new Date(date_3_hm);
		return date_3_hm_date;
	}

	public static String getStringToday() {
		Date currentTime = new Date();
		SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd HHmmss");
		String dateString = formatter.format(currentTime);
		return dateString;
	}

	public static String getStringTodayto() {
		Date currentTime = new Date();
		SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMddHHmmss");
		String dateString = formatter.format(currentTime);
		return dateString;
	}

	public static String getHour() {
		Date currentTime = new Date();
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String dateString = formatter.format(currentTime);
		String hour = dateString.substring(11, 13);
		return hour;
	}

	public static String getTime() {
		Date currentTime = new Date();
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String dateString = formatter.format(currentTime);
		String min = dateString.substring(14, 16);
		return min;
	}

	public static String getUserDate(String sformat) {
		Date currentTime = new Date();
		SimpleDateFormat formatter = new SimpleDateFormat(sformat);
		String dateString = formatter.format(currentTime);
		return dateString;
	}

	public static String getTwoHour(String st1, String st2) {
		String kk[] = null;
		String jj[] = null;
		kk = st1.split(":");
		jj = st2.split(":");
		if (Integer.parseInt(kk[0]) < Integer.parseInt(jj[0])) {
			return "0";
		} else {
			double y = Double.parseDouble(kk[0]) + Double.parseDouble(kk[1]) / 60D;
			double u = Double.parseDouble(jj[0]) + Double.parseDouble(jj[1]) / 60D;
			return y - u <= 0.0D ? "0" : (new StringBuilder()).append(y - u).append("").toString();
		}
	}

	public static String getTwoDay(String sj1, String sj2) {
		SimpleDateFormat myFormatter = new SimpleDateFormat("yyyy-MM-dd");
		long day = 0L;
		try {
			Date e = myFormatter.parse(sj1);
			Date mydate = myFormatter.parse(sj2);
			day = (e.getTime() - mydate.getTime()) / 0x5265c00L;
		} catch (Exception var7) {
			var7.printStackTrace();
			return "";
		}
		return (new StringBuilder()).append(day).append("").toString();
	}

	public static String getPreTime(String sj1, String jj) {
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String mydate1 = "";
		try {
			Date e = format.parse(sj1);
			long Time = e.getTime() / 1000L + (long) (Integer.parseInt(jj) * 60);
			e.setTime(Time * 1000L);
			mydate1 = format.format(e);
		} catch (Exception var7) {
			var7.printStackTrace();
		}
		return mydate1;
	}

	public static String getNextDay(String nowdate, String delay) {
		String mdate;
		SimpleDateFormat e = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		mdate = "";
		Date d = strToDate(nowdate);
		long myTime = d.getTime() / 1000L + Long.parseLong(delay) * 24L * 60L * 60L;
		d.setTime(myTime * 1000L);
		mdate = e.format(d);
		return mdate;
	}

	public static String getNextDaytoSen(String statrdate, String delay) {
		String mdate;
		SimpleDateFormat e = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		mdate = "";
		Date d = strToDate(statrdate, "yyyy-MM-dd HH:mm:ss");
		long myTime = d.getTime() / 1000L + Long.parseLong(delay) * 24L * 60L * 60L;
		d.setTime(myTime * 1000L);
		mdate = e.format(d);
		return mdate;
	}

	public static boolean isLeapYear(String ddate) {
		Date d = strToDate(ddate);
		GregorianCalendar gc = (GregorianCalendar) Calendar.getInstance();
		gc.setTime(d);
		int year = gc.get(1);
		return year % 400 != 0 ? year % 4 != 0 ? false : year % 100 != 0 : true;
	}

	public static String getEDate(String str) {
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		ParsePosition pos = new ParsePosition(0);
		Date strtodate = formatter.parse(str, pos);
		String j = strtodate.toString();
		String k[] = j.split(" ");
		return (new StringBuilder()).append(k[2]).append(k[1].toUpperCase()).append(k[5].substring(2, 4)).toString();
	}

	public static String getEndDateOfMonth(String dat) {
		String str = dat.substring(0, 8);
		String month = dat.substring(5, 7);
		int mon = Integer.parseInt(month);
		if (mon != 1 && mon != 3 && mon != 5 && mon != 7 && mon != 8 && mon != 10 && mon != 12) {
			if (mon != 4 && mon != 6 && mon != 9 && mon != 11) {
				if (isLeapYear(dat))
					str = (new StringBuilder()).append(str).append("29").toString();
				else
					str = (new StringBuilder()).append(str).append("28").toString();
			} else {
				str = (new StringBuilder()).append(str).append("30").toString();
			}
		} else {
			str = (new StringBuilder()).append(str).append("31").toString();
		}
		return str;
	}

	public static boolean isSameWeekDates(Date date1, Date date2) {
		Calendar cal1 = Calendar.getInstance();
		Calendar cal2 = Calendar.getInstance();
		cal1.setTime(date1);
		cal2.setTime(date2);
		int subYear = cal1.get(1) - cal2.get(1);
		if (0 == subYear) {
			if (cal1.get(3) == cal2.get(3))
				return true;
		} else if (1 == subYear && 11 == cal2.get(2)) {
			if (cal1.get(3) == cal2.get(3))
				return true;
		} else if (-1 == subYear && 11 == cal1.get(2) && cal1.get(3) == cal2.get(3))
			return true;
		return false;
	}

	public static String getSeqWeek() {
		Calendar c = Calendar.getInstance(Locale.CHINA);
		String week = Integer.toString(c.get(3));
		if (week.length() == 1)
			week = (new StringBuilder()).append("0").append(week).toString();
		String year = Integer.toString(c.get(1));
		return (new StringBuilder()).append(year).append(week).toString();
	}

	public static String getWeek(String sdate, String num) {
		Date dd = strToDate(sdate);
		Calendar c = Calendar.getInstance();
		c.setTime(dd);
		if (num.equals("1"))
			c.set(7, 2);
		else if (num.equals("2"))
			c.set(7, 3);
		else if (num.equals("3"))
			c.set(7, 4);
		else if (num.equals("4"))
			c.set(7, 5);
		else if (num.equals("5"))
			c.set(7, 6);
		else if (num.equals("6"))
			c.set(7, 7);
		else if (num.equals("0"))
			c.set(7, 1);
		return (new SimpleDateFormat("yyyy-MM-dd")).format(c.getTime());
	}

	public static String getWeek(String sdate) {
		Date date = strToDate(sdate);
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		return (new SimpleDateFormat("EEEE")).format(c.getTime());
	}

	public static String getWeekStr(String sdate) {
		String str = "";
		str = getWeek(sdate);
		if ("1".equals(str))
			str = "������";
		else if ("2".equals(str))
			str = "����һ";
		else if ("3".equals(str))
			str = "���ڶ�";
		else if ("4".equals(str))
			str = "������";
		else if ("5".equals(str))
			str = "������";
		else if ("6".equals(str))
			str = "������";
		else if ("7".equals(str))
			str = "������";
		return str;
	}

	public static long getDays(String date1, String date2) {
		if (date1 != null && !date1.equals("")) {
			if (date2 != null && !date2.equals("")) {
				SimpleDateFormat myFormatter = new SimpleDateFormat("yyyy-MM-dd");
				Date date = null;
				Date mydate = null;
				try {
					date = myFormatter.parse(date1);
					mydate = myFormatter.parse(date2);
				} catch (Exception var7) {
					var7.printStackTrace();
				}
				long day = (date.getTime() - mydate.getTime()) / 0x5265c00L;
				return day;
			} else {
				return 0L;
			}
		} else {
			return 0L;
		}
	}

	public static String getNowMonth(String sdate) {
		sdate = (new StringBuilder()).append(sdate.substring(0, 8)).append("01").toString();
		Date date = strToDate(sdate);
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		int u = c.get(7);
		String newday = getNextDay(sdate, (new StringBuilder()).append(1 - u).append("").toString());
		return newday;
	}

	public static long getDistinceMonth(String beforedate, String afterdate) throws ParseException {
		SimpleDateFormat d = new SimpleDateFormat("yyyy-MM-dd");
		long monthCount = 0L;
		try {
			Date e = d.parse(beforedate);
			Date d2 = d.parse(afterdate);
			monthCount = ((getYear(d2) - getYear(e)) * 12 + getMonth(d2)) - getMonth(e);
		} catch (ParseException var7) {
			var7.printStackTrace();
		}
		return monthCount;
	}

	public static long getDistinceDay(Date beforedate, Date afterdate) {
		long dayCount = 0L;
		dayCount = (beforedate.getTime() - afterdate.getTime()) / 0x5265c00L;
		return dayCount;
	}

	public static long getDistinceDay(String beforedate, String afterdate) throws ParseException {
		SimpleDateFormat d = new SimpleDateFormat("yyyy-MM-dd");
		long dayCount = 0L;
		try {
			Date e = d.parse(beforedate);
			Date d2 = d.parse(afterdate);
			dayCount = (d2.getTime() - e.getTime()) / 0x5265c00L;
		} catch (ParseException var7) {
			var7.printStackTrace();
		}
		return dayCount;
	}

	public static String disTime(Date date1, Date date2) throws ParseException {
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		return disTime(df.format(date1), df.format(date2));
	}

	public static int disDay(String date1, String date2) {
		int day = 0;
		try {
			SimpleDateFormat e = new SimpleDateFormat("yyyy-MM-dd");
			Date now = e.parse(date1);
			Date date = e.parse(date2);
			long l = now.getTime() - date.getTime();
			day = (int) (l / 0x5265c00L);
		} catch (Exception var8) {
			var8.printStackTrace();
		}
		return day;
	}

	public static double disDateTime(String date1, String date2) {
		double la = 0.0D;
		try {
			SimpleDateFormat e = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			Date now = e.parse(date1);
			Date date = e.parse(date2);
			long l = now.getTime() - date.getTime();
			long day = l / 0x5265c00L;
			long hour = l / 0x36ee80L - day * 24L;
			double min = l / 60000L - day * 24L * 60L - hour * 60L;
			double s = (double) (l / 1000L - day * 24L * 60L * 60L - hour * 60L * 60L) - min * 60D;
			la = (double) (day * 24L + hour) + min / 60D + s / 3600D;
		} catch (Exception var17) {
			var17.printStackTrace();
		}
		return la;
	}

	public static String disTime(String date1, String date2) throws ParseException {
		StringBuffer sb = new StringBuffer();
		long day;
		long hour;
		long min;
		SimpleDateFormat e = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date now = e.parse(date1);
		Date date = e.parse(date2);
		long l = now.getTime() - date.getTime();
		day = l / 0x5265c00L;
		hour = l / 0x36ee80L - day * 24L;
		min = l / 60000L - day * 24L * 60L - hour * 60L;
		if (day <= 0L && hour <= 0L && min <= 0L)
			return "1";
		try {
			if (day > 0L)
				sb.append((new StringBuilder()).append(day).append("��").toString());
			if (hour > 0L)
				sb.append((new StringBuilder()).append(hour).append("Сʱ").toString());
			if (min > 0L)
				sb.append((new StringBuilder()).append(min).append("").toString());
		} catch (Exception var14) {
			var14.printStackTrace();
		}
		return sb.toString();
	}

	public static Date getFirstDateOfMonth(Date d) {
		Calendar c = Calendar.getInstance();
		c.setTime(d);
		c.set(5, 1);
		c.set(11, 0);
		c.set(12, 0);
		c.set(13, 0);
		Date date = c.getTime();
		return date;
	}

	public static String formatDate(Date d, String pattern) {
		SimpleDateFormat sf = new SimpleDateFormat(pattern);
		return sf.format(d);
	}

	public static Date parseToDate(String sDate, String pattern) throws ParseException {
		SimpleDateFormat sf = new SimpleDateFormat(pattern);
		return sf.parse(sDate);
	}

	public static Date getMinTime(Date dt) {
		Date dt1 = null;
		try {
			dt1 = parseToDate(formatDate(dt, "yyyyMMdd"), "yyyyMMdd");
		} catch (ParseException var3) {
			var3.printStackTrace();
		}
		return dt1;
	}

	public static Date getMaxTime(Date dt) {
		Date dt1 = null;
		Calendar ca = Calendar.getInstance();
		ca.setTime(dt);
		ca.add(5, 1);
		dt1 = ca.getTime();
		dt1 = getMinTime(dt1);
		ca.setTime(dt1);
		ca.add(13, -1);
		dt1 = ca.getTime();
		return dt1;
	}

	public static Timestamp parseToTimestamp(Date date) {
		return new Timestamp(date.getTime());
	}

	public static String formatDate(Timestamp d, String pattern) {
		SimpleDateFormat sf = new SimpleDateFormat(pattern);
		return sf.format(d);
	}

	public static boolean isValidKey(String createKeyDate, long expire_time) {
		SimpleDateFormat dateformat;
		Date now;
		dateformat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		now = new Date();
		long time;
		try {
			Date e = dateformat.parse(createKeyDate);
			Date currentTime = dateformat.parse(dateformat.format(now));
			time = (currentTime.getTime() - e.getTime()) / 1000L;
		} catch (ParseException e1) {
			e1.printStackTrace();
			return false;
		}

		return time > expire_time;
	}

	public static Date compareDateInNull(Date createDate, String absDays, String sourceDate2) {
		String loseReadyDateStr = getNextDay(formatDate(createDate, "yyyy-MM-dd HH:mm:ss"), absDays.toString());
		Date loseReadyDate = toDate(loseReadyDateStr, "yyyy-MM-dd HH:mm:ss");
		loseReadyDate = getMaxTime(loseReadyDate);
		if ("null".equals(sourceDate2) && "0".equals(absDays))
			return getMaxTime(new Date());
		if ("null".equals(sourceDate2))
			return loseReadyDate;
		if ("0".equals(absDays)) {
			Date sourceDate = getMaxTime(toDate(sourceDate2, "yyyy-MM-dd HH:mm:ss"));
			return sourceDate;
		} else {
			Date sourceDate = getMaxTime(toDate(sourceDate2, "yyyy-MM-dd HH:mm:ss"));
			return loseReadyDate.after(sourceDate) ? sourceDate : loseReadyDate;
		}
	}

	public static boolean loseDate(Date loseDate) {
		Date nows = new Date();
		long hous = (nows.getTime() - loseDate.getTime()) / 1000L;
		return hous > 0L;
	}

	public static boolean betweenBeginAndEnd(Date paramDate, Date beginDate, Date endDate) {
		long bHous = (paramDate.getTime() - beginDate.getTime()) / 1000L;
		long eHous = (endDate.getTime() - paramDate.getTime()) / 1000L;
		return bHous > 0L && eHous > 0L;
	}

	public static Date compareDate(Date createDate, String absDays, Date sourceDate2) {
		sourceDate2 = getMaxTime(sourceDate2);
		String loseReadyDateStr = getNextDay(formatDate(createDate, "yyyy-MM-dd HH:mm:ss"), absDays.toString());
		Date loseReadyDate = toDate(loseReadyDateStr, "yyyy-MM-dd HH:mm:ss");
		loseReadyDate = getMaxTime(loseReadyDate);
		return loseReadyDate.after(sourceDate2) ? sourceDate2 : loseReadyDate;
	}

	public static String formatTime(int day, String standFormat) {
		String date = null;
		Calendar c = Calendar.getInstance();
		c.add(5, day);
		SimpleDateFormat format = new SimpleDateFormat(standFormat);
		date = format.format(c.getTime());
		return date;
	}

	public static String getTimeBeforeORAfter(int days, String pattern) {
		if (pattern == null)
			pattern = "yyyy-MM-dd HH:mm:ss";
		SimpleDateFormat sdf = new SimpleDateFormat(pattern);
		Calendar c = Calendar.getInstance();
		c.add(5, days);
		return sdf.format(c.getTime());
	}

	public static boolean isAfterToday(String tocompareStr) throws ParseException {
		String pattern = "yyyy-MM-dd HH:mm:ss";
		SimpleDateFormat sdf = new SimpleDateFormat(pattern);
		Date tocompareDate = sdf.parse(tocompareStr);
		return tocompareDate.after(Calendar.getInstance().getTime());
	}

	public static Long countDifSeconds(Timestamp t1, Timestamp t2) {
		return t1 == null || t2 == null ? Long.valueOf(0L)
				: t1.compareTo(t2) < 0 ? Long.valueOf((t2.getTime() - t1.getTime()) / 1000L)
						: Long.valueOf((t1.getTime() - t2.getTime()) / 1000L);
	}

	public static int getDateOfMonth() {
		Calendar c = Calendar.getInstance();
		return c.get(5);
	}

	public static String getTimeBeforeOrAfterSenconds(Timestamp sourceTime, long seconds) {
		if (sourceTime != null) {
			long time = sourceTime.getTime() + seconds * 1000L;
			sourceTime.setTime(time);
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			return sdf.format(sourceTime);
		} else {
			return "";
		}
	}

	public static String getTimeBeforeOrAfterMonth(int months, String pattern, Date sourceTime) {
		if (pattern == null)
			pattern = "yyyy-MM-dd HH:mm:ss";
		SimpleDateFormat sdf = new SimpleDateFormat(pattern);
		Calendar c = Calendar.getInstance();
		c.setTime(sourceTime);
		c.add(2, months);
		return sdf.format(c.getTime());
	}


	public static int getYear(Date date) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		return calendar.get(1);
	}

	public static int getMonth(Date date) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		return calendar.get(2);
	}

	public static int getMinute(Date date) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		return calendar.get(12);
	}

	public static int getSecond(Date date) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		return calendar.get(13);
	}

	public static String unicode2String(String unicode) {
		StringBuffer string = new StringBuffer();
		String hex[] = unicode.split("\\\\u");
		for (int i = 1; i < hex.length; i++) {
			int data = Integer.parseInt(hex[i], 16);
			string.append((char) data);
		}

		return string.toString();
	}

	public static String string2Unicode(String string) {
		StringBuffer unicode = new StringBuffer();
		for (int i = 0; i < string.length(); i++) {
			char c = string.charAt(i);
			unicode.append((new StringBuilder()).append("\\u").append(Integer.toHexString(c)).toString());
		}

		return unicode.toString();
	}
	public static void main(String[] args) {
		System.out.println(DateUtils.unicode2String("\\u70\\u72\\u6f\\u6a\\u65\\u63\\u74"));
	}
}
