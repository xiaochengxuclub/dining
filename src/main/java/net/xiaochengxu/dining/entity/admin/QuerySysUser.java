package net.xiaochengxu.dining.entity.admin;

import java.io.Serializable;

/**
*FileName: QuerySysUser.java
*@author lzd
*@version V1.0
*@Date: 2016年11月11日 下午1:11:50
*Description:后台用户 查询辅助类
*History:
*/
public class QuerySysUser implements Serializable{
	private static final long serialVersionUID = 1L;
	private String keyWord;
	public String getKeyWord() {
		return keyWord;
	}
	public void setKeyWord(String keyWord) {
		this.keyWord = keyWord;
	}
}
