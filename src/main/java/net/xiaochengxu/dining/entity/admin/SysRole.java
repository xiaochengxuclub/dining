package net.xiaochengxu.dining.entity.admin;

import java.io.Serializable;
import java.util.Date;

/**
*FileName: SysRole.java
*@author lzd
*@version V1.0
*@Date: 2016年11月11日 下午1:10:06
*Description:角色
*History:
*/
public class SysRole implements Serializable{
	
	private static final long serialVersionUID = 1L;
	private int roleId;
	private String roleName;
	private Date createTime;
	public int getRoleId() {
		return roleId;
	}
	public void setRoleId(int roleId) {
		this.roleId = roleId;
	}
	public String getRoleName() {
		return roleName;
	}
	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
}
