package net.xiaochengxu.dining.dao.website;


import java.util.List;

import net.xiaochengxu.dining.common.annotation.MyBatisDao;
import net.xiaochengxu.dining.entity.website.WebsiteProfile;

/**
*FileName: WebsiteProfileDao.java
*@author lzd
*@version V1.0
*@Date: 2016年11月11日 下午3:24:25
*Description:网站配置
*History:
*/
@MyBatisDao
public interface WebsiteProfileDao {
	/**
	 * 根据type查询网站配置
	 */
	public WebsiteProfile getWebsiteProfileByType(String type);
	/**
	 * 添加查询网站配置
	 */
	public void addWebsiteProfileByType(WebsiteProfile websiteProfile);
	/**
	 * 更新网站配置管理
	 */
	public void updateWebsiteProfile(WebsiteProfile websiteProfile);

	/**
	 * 查询网站配置
	 */
	public List<WebsiteProfile> getWebsiteProfileList();
}