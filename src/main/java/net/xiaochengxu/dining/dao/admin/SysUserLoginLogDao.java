package net.xiaochengxu.dining.dao.admin;

import java.util.List;
import java.util.Map;

import net.xiaochengxu.dining.common.annotation.MyBatisDao;
import net.xiaochengxu.dining.common.entity.PageEntity;
import net.xiaochengxu.dining.entity.admin.SysUserLoginLog;

/**
*FileName: SysUserLoginLogDao.java
*@author lzd
*@version V1.0
*@Date: 2016年11月11日 下午2:10:39
*Description:后台用户登录日志
*History:
*/
@MyBatisDao
public interface SysUserLoginLogDao {
	/**
	 * 添加登录日志
	 * @param loginLog
	 * @return 日志ID
	 */
	public int createLoginLog(SysUserLoginLog loginLog);
	
	/**
	 * 查询用户登录日志
	 * @param userId 用户ID
	 * @param page 分页条件
	 * @return List<SysUserLoginLog>
	 */
	public List<SysUserLoginLog> queryUserLogPage(Map<String,Object> map);

}
