package net.xiaochengxu.dining.dao.admin;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import net.xiaochengxu.dining.common.annotation.MyBatisDao;
import net.xiaochengxu.dining.common.entity.PageEntity;
import net.xiaochengxu.dining.entity.admin.SysUser;

/**
*Copyright ©, 2015-2018 国家数字复合出版系统工程
*FileName: SysUserDao.java
*@author lzd
*@version V1.0
*@Date: 2016年11月11日 下午2:10:19
*Description:
*History:
*/
@MyBatisDao
public interface SysUserDao {
	/***
	 * 创建用户
	 * @param sysuser 用户实体
	 * @return 用户ID
	 */
	public int createSysUser(SysUser sysuser);

	/**
	 * 更新用户信息
	 * @param sysuser 用户实体
	 */
	public void updateSysUser(SysUser sysuser);

	/**
	 * 通过ID，查询用户实体信息
	 * @param userId 用户ID
	 * @return SysUser
	 */
	public SysUser querySysUserByUserId(int userId);
	
	/**
	 * 分页查询用户列表
	 * @param querySysUser 查询条件
	 * @param page 分页条件
	 * @return 用户实体列表
	 */
	public List<SysUser> querySysUserPage(Map<String,Object> map);

	/**
	 * 验证用户帐户是否存在
	 * @param userLoginName
	 */
	public int validateLoginName(String userLoginName);
	
	/**
	 * 查询登录用户
	 * @param sysUser 查询条件
	 * @return SysUser
	 */
	public SysUser queryLoginUser(SysUser sysUser);
	
	/**
	 * 修改用户密码
	 * @param sysUser
	 */
	public void updateUserPwd(SysUser sysUser);
	
	/**
	 *禁用或启用后台用户 
	 * @param map 修改条件  userId 用户ID type 1启用 2禁用
	 */
	public void updateDisableOrstartUser(Map<String,Object> map);
	
	/***
	 * 修改用户登录最后登录时间和IP
	 * @param map
	 */
	public void updateUserLoginLog(Map<String,Object> map);
}
