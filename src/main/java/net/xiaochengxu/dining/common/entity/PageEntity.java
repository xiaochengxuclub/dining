package net.xiaochengxu.dining.common.entity;

import java.io.Serializable;

public class PageEntity implements Serializable {

	private static final long serialVersionUID = 1L;
	private int currentPage;
	private int pageSize;
	private int totalResultSize;
	private int totalPageSize;
	private boolean first;
	private boolean last;

	public PageEntity() {
		currentPage = 1;
		pageSize = 10;
		first = false;
		last = false;
	}

	public int getCurrentPage() {
		return currentPage;
	}

	public void setCurrentPage(int currentPage) {
		this.currentPage = currentPage;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public int getTotalResultSize() {
		return totalResultSize;
	}

	public void setTotalResultSize(int totalResultSize) {
		this.totalResultSize = totalResultSize;
	}

	public int getTotalPageSize() {
		return totalPageSize;
	}

	public void setTotalPageSize(int totalPageSize) {
		this.totalPageSize = totalPageSize;
	}

	public boolean isFirst() {
		return getCurrentPage() <= 1;
	}

	public void setFirst(boolean first) {
		this.first = first;
	}

	public boolean isLast() {
		return getCurrentPage() >= getTotalPageSize();
	}

	public void setLast(boolean last) {
		this.last = last;
	}

}
