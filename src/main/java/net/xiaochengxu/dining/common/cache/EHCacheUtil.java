package net.xiaochengxu.dining.common.cache;

import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;
import net.xiaochengxu.dining.common.constants.CacheConstans;
import net.xiaochengxu.dining.utils.DateUtils;
import net.xiaochengxu.dining.utils.ObjectUtils;

/**
*FileName: EHCacheUtil.java
*@author lzd
*@version V1.0
*@Date: 2016年11月11日 下午4:31:23
*Description:缓存工具
*History:
*/
public class EHCacheUtil {

	private static CacheManager cacheManager = null;
	private static Cache cache = null;

	public EHCacheUtil() {
	}

	public static CacheManager initCacheManager() {
		try {
			if (cacheManager == null) {
				cacheManager = CacheManager.create();
				cache = new Cache("objectCache", 10000, true, false, 3600L, 3600L);
				cacheManager.addCache(cache);
			}
		} catch (Exception var1) {
			var1.printStackTrace();
		}
		return cacheManager;
	}

	public static Object get(String key) {
		if (ObjectUtils.isNotNull(cache) && ObjectUtils.isNotNull(cache.get(key)))
			return cache.get(key).getObjectValue();
		return "";
	}

	public static void set(String key, Object value) {
		try {
			if (cache != null)
				cache.put(new Element(key, value));
		} catch (Exception var3) {
			var3.printStackTrace();
		}
	}

	public static boolean remove(String key) {
		if (cache != null)
			return cache.remove(key);
		return false;
	}

	public static boolean removeAll() {
		try {
			if (cache != null)
				cache.removeAll();
		} catch (Exception var1) {
			var1.printStackTrace();
		}
		return false;
	}

	public static void set(String key, Object value, int exp) {
		try {
			if (cache != null) {
				Element e = new Element(key, value);
				e.setTimeToLive(exp);
				cache.put(e);
			}
		} catch (Exception var4) {
			var4.printStackTrace();
		}
	}

	static {
		initCacheManager();
	}
	public static void main(String[] args) {
		EHCacheUtil.set(CacheConstans.LOGIN_MEMCACHE_PREFIX, 111);
		Object o = EHCacheUtil.get(CacheConstans.LOGIN_MEMCACHE_PREFIX);
		System.out.println(o.toString());
	}
}
