/**
 * 
 */
package net.xiaochengxu.dining.common.constants;

/**
 * @author vector
 *
 */
public class ContentType {
	
	public static final String JSON = "application/json";
	
	public static final String XML = "application/xml";

}
