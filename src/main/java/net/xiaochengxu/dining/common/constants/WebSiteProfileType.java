package net.xiaochengxu.dining.common.constants;
/**
*FileName: WebSiteProfileType.java
*@author lzd
*@version V1.0
*@Date: 2016年11月11日 下午4:33:10
*Description:网站管理常量
*History:
*/
public enum WebSiteProfileType {
    web,//网站常规配置
    logo,
    guide,//导航
    censusCode,//统计代码
    ico//ico文件
}
