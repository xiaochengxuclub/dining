package net.xiaochengxu.dining.controller.admin;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import net.xiaochengxu.dining.common.cache.EHCacheUtil;
import net.xiaochengxu.dining.common.constants.CacheConstans;
import net.xiaochengxu.dining.common.controller.BaseController;
import net.xiaochengxu.dining.entity.admin.SysFunction;
import net.xiaochengxu.dining.entity.admin.SysUser;
import net.xiaochengxu.dining.service.admin.SysFunctionService;
import net.xiaochengxu.dining.utils.ObjectUtils;
import net.xiaochengxu.dining.utils.SingletonLoginUtils;
import net.xiaochengxu.dining.utils.WebUtils;
/**
*FileName: MainController.java
*@author lzd
*@version V1.0
*@Date: 2016年11月11日 下午2:37:32
*Description:后台首页控制器
*History:
*/
@Controller
@RequestMapping("/admin/main")
public class MainController extends BaseController{
	private static final Logger logger = LoggerFactory.getLogger(MainController.class);
	private static String mainPage = getViewPath("/admin/main/main");//后台管理主界面
	private static String mainIndexPage = getViewPath("/admin/main/index");//后台操作中心初始化首页
	@Autowired
	private SysFunctionService sysFunctionService;
	
	private List<SysFunction> functionList =null;
	/**
	 * 进入操作中心
	 * @param request
	 * @return ModelAndView
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping()
	public ModelAndView mainPage(HttpServletRequest request,HttpServletResponse response){
		ModelAndView model = new ModelAndView();
		try{
			SysUser sysuser = SingletonLoginUtils.getLoginSysUser(request);
			//得到缓存用户权限
			List<SysFunction> userFunctionList = new ArrayList<SysFunction>();
			Object userFunctionListObject =EHCacheUtil.get(CacheConstans.USER_FUNCTION_PREFIX+sysuser.getUserId());
			//如果缓存中不存在则查询数据库中的用户权限
			if(ObjectUtils.isNull(userFunctionListObject)){
				userFunctionList = sysFunctionService.querySysUserFunction(sysuser.getUserId());
				EHCacheUtil.set(CacheConstans.USER_FUNCTION_PREFIX+sysuser.getUserId(), userFunctionList);
			}else{
				userFunctionList = (List<SysFunction>)userFunctionListObject;
			}
			
			//处理权限
			functionList = handleUserFunction(userFunctionList);
			model.addObject("userFunctionList", functionList);
			model.setViewName(mainPage);
			model.addObject("sysuser", sysuser);
		}catch (Exception e) {
			model.setViewName(this.setExceptionRequest(request, e));
			logger.error("mainPage()--error",e);
		}
		return model;
	}
	
	/**
	 * 处理权限
	 * @param userFunctionList 用户权限列表
	 * @return List<Map<String,Object>>
	 */
	private List<SysFunction> handleUserFunction(List<SysFunction> userFunctionList){
		functionList = new ArrayList<SysFunction>();
		List<SysFunction> _functionList = null;
		if(userFunctionList!=null && userFunctionList.size()>0){
			//获取根级权限
			List<SysFunction> parentList = new ArrayList<SysFunction>();
			for(SysFunction sf : userFunctionList){
				int _index=0;
				for(SysFunction _sf : userFunctionList){
					if(sf.getParentId()==_sf.getFunctionId()){//筛选父级（parentId不存在，或者为零）
						_index++;
						break;
					}
				}
				if(_index==0 && sf.getFunctionType()==1){//functionType 1菜单 2功能
					parentList.add(sf);
				}
			}
			//获取根级权限的子级权限
			for(SysFunction psf : parentList){
				recursionFunction(userFunctionList,psf);
			}
			_functionList =parentList;
		}
		return _functionList;
	}
	
	/**
	 * 递归得到每个权限的子级权限
	 * @param userFunctionList 用户权限列表
	 * @param sf 当前权限
	 */
	private void recursionFunction(List<SysFunction> userFunctionList,SysFunction sf){
		List<SysFunction> childFunction = new ArrayList<SysFunction>();
		for(SysFunction usf : userFunctionList){
			if(sf.getFunctionId()==usf.getParentId() && usf.getFunctionType()==1){
				childFunction.add(usf);
				recursionFunction(userFunctionList,usf);
			}
		}
		sf.setChildList(childFunction);
		functionList.add(sf);
	}

	/**
	 * 后台操作中心初始化首页
	 * @param request
	 * @return ModelAndView
	 */
	@RequestMapping("/index")
	public ModelAndView mainIndex(HttpServletRequest request){
		ModelAndView model = new ModelAndView();
		try{
			// 今天登录人数数据获得
			model.addObject("todayloginnum", 10);
			model.setViewName(mainIndexPage);
			
			//Map<String, Object> webCountMap=(Map<String, Object>) EHCacheUtil.get(CacheConstans.WEB_COUNT);
			Map<String, Object> webCountMap = null;
			if(webCountMap==null){
				webCountMap=new HashMap<String, Object>();
				//所有文章数量
				webCountMap.put("articleCount", 1);//所有文章数量
				//所有用户数量
				webCountMap.put("userCount", 2);//所有用户数量
				//所有课程数量
				webCountMap.put("courseCount", 10);//所有课程数量
				//所有问答数
				webCountMap.put("questionsCount", 5);//所有问答数
				EHCacheUtil.set(CacheConstans.WEB_COUNT, webCountMap.toString(),CacheConstans.WEB_COUNT_TIME);
				model.addObject("webCountMap",webCountMap);
			}else{
				model.addObject("webCountMap",webCountMap);
			}
		}catch (Exception e) {
			model.setViewName(this.setExceptionRequest(request, e));
			logger.error("mainIndex()---error",e);
		}
		return model;
	}
	
	/**
	 * 访问权限受限制跳转
	 * @return ModelAndView
	 */
	@RequestMapping("/notfunction")
	public ModelAndView notFunctionMsg(){
		ModelAndView model = new ModelAndView();
		model.addObject("message", "对不起，您没有操作权限！");
		model.setViewName("/common/notFunctonMsg");
		return model;
	}
	
}
