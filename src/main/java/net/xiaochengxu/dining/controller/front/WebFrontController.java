package net.xiaochengxu.dining.controller.front;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import net.xiaochengxu.dining.common.controller.BaseController;

@Controller
public class WebFrontController extends BaseController {

	private static final Logger logger = LoggerFactory.getLogger(WebFrontController.class);

	/**
	 * 首页获取网站首页数据
	 */
	@RequestMapping("/")
	public String getIndexpage(HttpServletRequest request, Model model) {
		return getViewPath("/front/main/index");//首页页面
	}

}
