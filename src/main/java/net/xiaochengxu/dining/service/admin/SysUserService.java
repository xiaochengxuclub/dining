package net.xiaochengxu.dining.service.admin;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import net.xiaochengxu.dining.common.entity.PageEntity;
import net.xiaochengxu.dining.common.entity.PageOL;
import net.xiaochengxu.dining.dao.admin.SysUserDao;
import net.xiaochengxu.dining.entity.admin.QuerySysUser;
import net.xiaochengxu.dining.entity.admin.SysUser;

/**
*FileName: SysUserService.java
*@author lzd
*@version V1.0
*@Date: 2016年11月11日 下午4:35:12
*Description:后台用户
*History:
*/
@Service("sysUserService")
public class SysUserService {
	
	@Autowired
	private SysUserDao sysUserDao;
	
	
	public int createSysUser(SysUser sysuser) {
		return sysUserDao.createSysUser(sysuser);
	}

	
	public void updateSysUser(SysUser sysuser) {
		sysUserDao.updateSysUser(sysuser);
	}

	
	public SysUser querySysUserByUserId(int userId) {
		return sysUserDao.querySysUserByUserId(userId);
	}

	
	public List<SysUser> querySysUserPage(QuerySysUser querySysUser,
			PageEntity page) {
		PageOL pageOL = new PageOL();
	    pageOL.setOffsetPara((page.getCurrentPage() - 1) * page.getPageSize());
	    pageOL.setLimitPara(page.getPageSize());
		Map<String,Object> map = new HashMap<String, Object>();
		map.put("keyWord", querySysUser.getKeyWord());
		map.put("pageOL", pageOL);
		return sysUserDao.querySysUserPage(map);
	}

	
	public boolean validateLoginName(String userLoginName) {
		int count = sysUserDao.validateLoginName(userLoginName);
		if(count<=0){
			return true;
		}
		return false;
	}

	
	public SysUser queryLoginUser(SysUser sysUser) {
		return sysUserDao.queryLoginUser(sysUser);
	}

	
	public void updateUserPwd(SysUser sysUser) {
		sysUserDao.updateUserPwd(sysUser);
	}

	
	public void updateDisableOrstartUser(int userId, int type) {
		Map<String,Object> map = new HashMap<String, Object>();
		map.put("userId", userId);
		map.put("type", type);
		sysUserDao.updateDisableOrstartUser(map);
	}

	
	public void updateUserLoginLog(int userId, Date time, String ip) {
		Map<String,Object> map = new HashMap<String, Object>();
		map.put("userId", userId);
		map.put("time", time);
		map.put("ip", ip);
		sysUserDao.updateUserLoginLog(map);
	}
	
}
