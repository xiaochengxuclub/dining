package net.xiaochengxu.dining.service.admin;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import net.xiaochengxu.dining.common.entity.PageEntity;
import net.xiaochengxu.dining.common.entity.PageOL;
import net.xiaochengxu.dining.dao.admin.SysUserLoginLogDao;
import net.xiaochengxu.dining.entity.admin.SysUserLoginLog;

/**
*FileName: SysUserLoginLogService.java
*@author lzd
*@version V1.0
*@Date: 2016年11月11日 下午4:34:59
*Description:后台用户登录日志
*History:
*/
@Service("sysUserLoginLogService")
public class SysUserLoginLogService{

	@Autowired
	private SysUserLoginLogDao sysUserLoginLogDao;
	
	public int createLoginLog(SysUserLoginLog loginLog) {
		return sysUserLoginLogDao.createLoginLog(loginLog);
	}
	
	public List<SysUserLoginLog> queryUserLogPage(int userId, PageEntity page) {
		PageOL pageOL = new PageOL();
	    pageOL.setOffsetPara((page.getCurrentPage() - 1) * page.getPageSize());
	    pageOL.setLimitPara(page.getPageSize());
		Map<String,Object> map = new HashMap<String, Object>();
		map.put("userId", userId);
		map.put("pageOL", pageOL);
		return sysUserLoginLogDao.queryUserLogPage(map);
	}

}
