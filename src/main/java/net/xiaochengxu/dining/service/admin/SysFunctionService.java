package net.xiaochengxu.dining.service.admin;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import net.xiaochengxu.dining.dao.admin.SysFunctionDao;
import net.xiaochengxu.dining.entity.admin.SysFunction;

/**
*FileName: SysFunctionService.java
*@author lzd
*@version V1.0
*@Date: 2016年11月11日 下午4:34:29
*Description:后台系统权限
*History:
*/
@Service("sysFunctionService")
public class SysFunctionService {

	@Autowired
	private SysFunctionDao sysFunctionDao;
	
	public List<SysFunction> queryAllSysFunction() {
		return sysFunctionDao.queryAllSysFunction();
	}
	
	public int cresateSysFunction(SysFunction sysFunction) {
		return sysFunctionDao.cresateSysFunction(sysFunction);
	}
	
	public void updateFunction(SysFunction sysFunction) {
		sysFunctionDao.updateFunction(sysFunction);
		
	}
	
	public void updateFunctionParentId(int parentId, int functionId) {
		Map<String,Object> paramrs=new HashMap<String, Object>();
		paramrs.put("parentId", parentId);
		paramrs.put("functionId", functionId);
		sysFunctionDao.updateFunctionParentId(paramrs);
		
	}
	
	public void deleteFunctionByIds(String ids) {
		if(ids!=null && ids.trim().length()>0){
			if(ids.trim().endsWith(",")){
				ids = ids.trim().substring(0,ids.trim().length()-1);
			}
			sysFunctionDao.deleteFunctionByIds(ids);
		}
	}
	
	
	public List<SysFunction> querySysUserFunction(int userId) {
		return sysFunctionDao.querySysUserFunction(userId);
	}

}
