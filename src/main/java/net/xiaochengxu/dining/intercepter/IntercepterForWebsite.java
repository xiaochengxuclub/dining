package net.xiaochengxu.dining.intercepter;



import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import net.xiaochengxu.dining.common.constants.CommonConstants;
import net.xiaochengxu.dining.common.constants.WebSiteProfileType;
import net.xiaochengxu.dining.service.website.WebsiteProfileService;

/**
 * 网站配置管理拦截器
 */
public class IntercepterForWebsite extends HandlerInterceptorAdapter{
	 //logger
	 Logger logger = LoggerFactory.getLogger(IntercepterForWebsite.class);
     @Autowired
     private WebsiteProfileService websiteProfileService;
 	
     public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
         super.afterCompletion(request, response, handler, ex);
     }

     
     public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
         super.postHandle(request, response, handler, modelAndView);
     }
     
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
         try{
        	 //网站路径
        	 request.setAttribute("ctx",CommonConstants.contextPath);
        	 request.setAttribute("ctximg",CommonConstants.staticServer);
        	//获得网站配置
          	Map<String,Object> websitemap=websiteProfileService.getWebsiteProfileByType(WebSiteProfileType.web.toString());
          	request.setAttribute("websitemap",websitemap);
            //获得LOGO配置
          	Map<String,Object> logomap=websiteProfileService.getWebsiteProfileByType(WebSiteProfileType.logo.toString());
          	request.setAttribute("logomap",logomap);
          	//网站统计代码
            Map<String,Object> tongjiemap=websiteProfileService.getWebsiteProfileByType(WebSiteProfileType.censusCode.toString());
            request.setAttribute("tongjiemap",tongjiemap);
            
         }catch(Exception e){
        	 logger.error("LimitIntercepterForWebsite.preHandle 网站配置出错",e);
         }
		
    	return super.preHandle(request, response, handler);
    } 
}
