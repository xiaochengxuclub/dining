/**
 * 
 */
package net.xiaochengxu.dining.api.support;

import org.springframework.context.annotation.Bean;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * @author vector
 *
 */
@EnableSwagger2
public class ApiDocConfig {

	@Bean
	public Docket customDocket() {
		return new Docket(DocumentationType.SWAGGER_2)  
                .apiInfo(apiInfo())  
                .select()  
                .apis(RequestHandlerSelectors.basePackage("net.xiaochengxu.dining.api.controller"))  
                .paths(PathSelectors.any())  
                .build();
	}

	private ApiInfo apiInfo() {
		return new ApiInfoBuilder()  
				.title("我爱微信小程序之订餐App接口文档")
				.description("该项目用于订餐业务开源项目，只用于学习研究使用。")
                .termsOfServiceUrl("http://blog.csdn.net/he90227")  
//                .contact(new Contact("我爱小程序开发组", "https://git.oschina.net/xiaochengxuclub/dining", ""))  
                .version("1.0.0")  
                .build(); 
	}

}
