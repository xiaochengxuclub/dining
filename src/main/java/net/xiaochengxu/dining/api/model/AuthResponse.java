/**
 * 
 */
package net.xiaochengxu.dining.api.model;

/**
 * @author vector
 *
 */
public class AuthResponse {
	
	private String token;
	
	private User user;

	/**
	 * @return the token
	 */
	public String getToken() {
		return token;
	}

	/**
	 * @param token the token to set
	 */
	public void setToken(String token) {
		this.token = token;
	}

	/**
	 * @return the user
	 */
	public User getUser() {
		return user;
	}

	/**
	 * @param user the user to set
	 */
	public void setUser(User user) {
		this.user = user;
	}
	
}
