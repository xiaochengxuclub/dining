/**
 * 
 */
package net.xiaochengxu.dining.api.controller;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import net.xiaochengxu.dining.api.model.AuthInfo;
import net.xiaochengxu.dining.api.model.AuthResponse;
import net.xiaochengxu.dining.common.constants.ContentType;

/**
 * @author vector
 *
 */
@Api(tags = { "User" })
@RestController
@RequestMapping("User")
public class UserController {

	@ResponseStatus(code = HttpStatus.CREATED)
	@PostMapping(value = "auth", consumes = { ContentType.JSON }, produces = { ContentType.JSON })
	public AuthResponse auth(@RequestBody AuthInfo authInfo) {
		// TODO
		// 1. verify userinfo
		// 2. return token and userInfo
		System.err.println(authInfo);
		AuthResponse authResponse = new AuthResponse();
		authResponse.setToken("token");
		return authResponse;
	}

}
