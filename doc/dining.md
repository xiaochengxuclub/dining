该项目用于订餐业务开源项目，只用于学习研究使用。
### 技术框架
核心框架：Spring Framework4.1.4
视图框架：Spring MVC
持久层框架：MyBatis 3.2.8
JS框架：jQuery
富文本：kindeditor

### 开发环境
建议开发者使用以下环境，这样避免版本带来的问题
IDE:eclipse Version: Mars.1 Release (4.5.1)
DB:Mysql5.5
JDK:JAVA 8
tomcat:tomcat 8.0.30

### 已实现功能
目前已实现后台的权限和网站配置管理
演示站:http://127.0.0.1:8080/dining/admin
账号:admin
密码:111111

 _后期会把后台调整为bootstrap3样式_ ，有些需要改正的，请大家给我提出建议。

