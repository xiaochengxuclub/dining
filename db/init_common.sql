-- ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- -----标题:  数据库结构SQL
-- -----须知:  所有关键字大写；所有表名、列表、索引名等小写；禁止驼峰标识，使用分隔符"_"；禁止使用tab（以4个空格代替）；
-- ----------:  禁止使用数据库外键约束；必须有主键、唯一键约束；禁止使用mysql关键字作为表名、列名等；列一般必须为not null；字符集一般：utf8  utf8_general_ci
-- 列名规范:  1.所有表的主键名称均为id；2.基础表的id的类型设置为int类型，其他业务表为bigint，均为无符号：unsigned；3.字段的名称一般禁止使用拼音，如果长度过长，则使用各单词的缩写，最大长度不得超过26；
-- ---------:  4.枚举型的变量使用tinyint类型
-- -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

-- ----------------------------
-- 新建库
-- ----------------------------

CREATE DATABASE dining DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;

-- ----------------------------
-- 在新建的库中执行
-- ----------------------------

use dining;

-- ----------------------------
-- 菜品基本信息表
-- Table structure for bas_food_info
-- global
-- 2016-11-14 Glenn
-- ----------------------------
DROP TABLE IF EXISTS bas_food_info;
CREATE TABLE bas_food_info (
  id int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  name varchar(25) NOT NULL COMMENT '菜品名',
  tag_name varchar(100) COMMENT '标签名',
  type_id int(10) unsigned NOT NULL COMMENT '菜品分类',
  unit varchar(10) NOT NULL COMMENT '单位',
  price decimal(12,2) NOT NULL COMMENT '非会员价',
  member_price decimal(12,2) NOT NULL COMMENT '会员价',
  sort int(10) NOT NULL DEFAULT '0' COMMENT '排序',
  liked_sum int(10) NOT NULL DEFAULT '0' COMMENT '点赞数',
  sales int(10) NOT NULL DEFAULT '0' COMMENT '销量',
  ava_num int(10) NOT NULL DEFAULT '0' COMMENT '库存',
  is_discount tinyint(5) NOT NULL DEFAULT '0' COMMENT '是否折扣（0不折扣；1折扣）',
  info varchar(50)  COMMENT '菜品简介',
  img varchar(255)	COMMENT '菜品图片',
  status tinyint(5) NOT NULL DEFAULT '1' COMMENT '是否上架（0下架；1上架）',
  create_time datetime NOT NULL COMMENT '创建时间',
  creator_id int(10) unsigned NOT NULL COMMENT '创建人',
  modified_time datetime  COMMENT '修改时间',
  modifier_id int(10) unsigned COMMENT '修改人',
  PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Dumping data for table `bas_food_info`
-- ----------------------------
insert into bas_food_info (id, name,tag_name, type_id, unit, price, member_price, sort,info, create_time, creator_id) values (1, '香辣干鱼锅（2-3人量）','巴沙鱼GAP良好农业操作认证', 1, '锅', 128,118, 1, '主厨精选','2016-11-15 13:47:21',1),(2, '麻辣鲜鱼锅（2-3人量）','巴沙鱼GAP良好农业操作认证', 1, '锅', 98,88, 2, '主厨精选','2016-11-15 13:47:21',1),(3, '川香酸菜鱼锅（2-3人量）','巴沙鱼GAP良好农业操作认证', 1, '锅', 158,148, 3, '主厨精选','2016-11-15 13:47:21',1);
						
-- ----------------------------
-- 菜品种类表
-- Table structure for bas_menu_type
-- global
-- 2016-11-14 Glenn
-- ----------------------------
DROP TABLE IF EXISTS bas_menu_type;
CREATE TABLE bas_menu_type (
  id int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  name varchar(25) NOT NULL COMMENT '菜单名',
  sort int(10) NOT NULL DEFAULT '0' COMMENT '排序',
  status tinyint(5) DEFAULT '1' COMMENT '菜单状态（0不可用；1可用）',
  create_time datetime NOT NULL COMMENT '创建时间',
  creator_id int(10) unsigned NOT NULL COMMENT '创建人',
  modified_time datetime  COMMENT '修改时间',
  modifier_id int(10) unsigned COMMENT '修改人',
  PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Dumping data for table `bas_menu_type`
-- ----------------------------
insert into bas_menu_type(id,name,sort,create_time,creator_id) values (1,'特色鱼锅','1','2015-11-15',1);

-- ----------------------------
-- 菜品口味表
-- Table structure for bas_food_flaver
-- global
-- 2016-11-14 Glenn
-- ----------------------------
DROP TABLE IF EXISTS bas_food_flaver;
CREATE TABLE bas_food_flaver (
  id int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  name varchar(25) NOT NULL COMMENT '口味名称',
  sort int(10) NOT NULL DEFAULT '0' COMMENT '排序',
  status tinyint(5) DEFAULT '1' COMMENT '状态（0不可用；1可用）',
  create_time datetime NOT NULL COMMENT '创建时间',
  creator_id int(10) unsigned NOT NULL COMMENT '创建人',
  modified_time datetime  COMMENT '修改时间',
  modifier_id int(10) unsigned COMMENT '修改人',
  PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Dumping data for table `bas_food_flaver`
-- ----------------------------
insert into bas_food_flaver(id,name,sort,create_time, creator_id) values(1,'不辣','1','2015-11-15',1),(2,'微辣','2','2015-11-15',1),(3,'中辣','3','2015-11-15',1),(4,'特辣','4','2015-11-15',1),(5,'魔鬼辣','5','2015-11-15',1);

-- ----------------------------
-- 菜品备注表
-- Table structure for bas_food_note
-- global
-- 2016-11-14 Glenn
-- ----------------------------
DROP TABLE IF EXISTS bas_food_note;
CREATE TABLE bas_food_note (
  id int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  name varchar(25) NOT NULL COMMENT '备注名称',
  sort int(10) NOT NULL DEFAULT '0' COMMENT '排序',
  status tinyint(5) DEFAULT '1' COMMENT '状态（0不可用；1可用）',
  create_time datetime NOT NULL COMMENT '创建时间',
  creator_id int(10) unsigned NOT NULL COMMENT '创建人',
  modified_time datetime  COMMENT '修改时间',
  modifier_id int(10) unsigned COMMENT '修改人',
  PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Dumping data for table `bas_food_note`
-- ----------------------------
insert into bas_food_note(id,name,sort,create_time,creator_id) values (1,'不要香菜','1','2015-11-15',1), (2,'不要蒜','2','2015-11-15',1), (3,'不放芝麻','3','2015-11-15',1), (4,'不放葱','4','2015-11-15',1), (5,'不放姜','5','2015-11-15',1), (6,'二分之一','6','2015-11-15',1), (7,'少油','7','2015-11-15',1), (8,'少盐','8','2015-11-15',1), (9,'回民','9','2015-11-15',1), (10,'口味重点','10','2015-11-15',1), (11,'不放鸡精','11','2015-11-15',1), (12,'不放麻','12','2015-11-15',1);

-- ----------------------------
-- 菜品备注关联表
-- Table structure for food_note_assign
-- global
-- 2016-11-14 Glenn
-- ----------------------------
DROP TABLE IF EXISTS food_note_assign;
CREATE TABLE food_note_assign (
  id int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  food_id int(10) unsigned NOT NULL COMMENT '菜品id',
  note_id int(10) unsigned NOT NULL COMMENT '备注id',
  status tinyint(5) DEFAULT '1' COMMENT '状态（0不可用；1可用）',
  create_time datetime NOT NULL COMMENT '创建时间',
  creator_id int(10) unsigned NOT NULL COMMENT '创建人',
  modified_time datetime  COMMENT '修改时间',
  modifier_id int(10) unsigned COMMENT '修改人',
  PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Dumping data for table `food_note_assign`
-- ----------------------------
insert into  food_note_assign (id,food_id,note_id,create_time,creator_id) values (1,1,1,'2015-11-15',1),(2,1,2,'2015-11-15',1),(3,1,3,'2015-11-15',1);

-- ----------------------------
-- 菜品口味关联表
-- Table structure for food_flaver_assign
-- global
-- 2016-11-14 Glenn
-- ----------------------------
DROP TABLE IF EXISTS food_flaver_assign;
CREATE TABLE food_flaver_assign (
  id int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  food_id int(10) unsigned NOT NULL COMMENT '菜品id',
  flaver_id int(10) unsigned NOT NULL COMMENT '口味id',
  status tinyint(5) DEFAULT '1' COMMENT '状态（0不可用；1可用）',
  create_time datetime NOT NULL COMMENT '创建时间',
  creator_id int(10) unsigned NOT NULL COMMENT '创建人',
  modified_time datetime  COMMENT '修改时间',
  modifier_id int(10) unsigned COMMENT '修改人',
  PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Dumping data for table `food_flaver_assign`
-- ----------------------------
insert into  food_flaver_assign (id,food_id,flaver_id,create_time,creator_id) values (1,1,1,'2015-11-15',1),(2,1,2,'2015-11-15',1),(3,1,3,'2015-11-15',1);
