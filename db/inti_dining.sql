-- MySQL dump 10.13  Distrib 5.6.24, for Win32 (x86)
--
-- Host: localhost    Database: dining
-- ------------------------------------------------------
-- Server version	5.5.9

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `dining_website_profile`
--

DROP TABLE IF EXISTS `dining_website_profile`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dining_website_profile` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `TYPE` varchar(20) NOT NULL DEFAULT '' COMMENT '类型',
  `DESCIPTION` text COMMENT '内容JSON格式',
  `EXPLAIN` varchar(50) DEFAULT '' COMMENT '说明',
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dining_website_profile`
--

LOCK TABLES `dining_website_profile` WRITE;
/*!40000 ALTER TABLE `dining_website_profile` DISABLE KEYS */;
INSERT INTO `dining_website_profile` VALUES (1,'web','{\"copyright\":\"©2010-2015 52xiaochengxu有限公司 版权所有 京ICP备xxxxx号\",\"keywords\":\"微信小程序\",\"phone\":\"400-006-5079\",\"author\":\"52xiaochengxu(http://www.52xiaochengxu.com.cn)\",\"description\":\"52xiaochengxu是一家专注提供专业的微信小程序案例方案的专业服务。\",\"company\":\"微信小程序\",\"title\":\"中国微信小程序第一案例发布网站\",\"workTime\":\"9:00-18:00\",\"email\":\"test@qq.com\"}','基本信息的维护'),(3,'logo','{\"url\":\"/images/upload/websiteLogo/20161019/1476868413634.png\"}','logo'),(4,'censusCode','{\"censusCodeString\":\"\"}','统计代码');
/*!40000 ALTER TABLE `dining_website_profile` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_function`
--

DROP TABLE IF EXISTS `sys_function`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_function` (
  `FUNCTION_ID` int(11) NOT NULL AUTO_INCREMENT COMMENT '权限ID',
  `PARENT_ID` int(11) DEFAULT '0' COMMENT '权限父ID',
  `FUNCTION_NAME` varchar(100) DEFAULT NULL COMMENT '权限名',
  `FUNCTION_URL` varchar(255) DEFAULT NULL COMMENT '权限URL',
  `FUNCTION_TYPE` tinyint(1) DEFAULT '0' COMMENT '权限类型 1菜单 2功能',
  `CREATE_TIME` timestamp NULL DEFAULT NULL COMMENT '创建时间',
  `SORT` int(11) DEFAULT '0' COMMENT '排序',
  PRIMARY KEY (`FUNCTION_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=141 DEFAULT CHARSET=utf8 COMMENT='权限表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_function`
--

LOCK TABLES `sys_function` WRITE;
/*!40000 ALTER TABLE `sys_function` DISABLE KEYS */;
INSERT INTO `sys_function` VALUES (18,45,'权限树','/admin/sysfunctioin/showfunctionztree',1,'2015-03-17 05:46:16',0),(19,45,'角色管理','/admin/sysrole/showroleList',1,'2015-03-17 05:46:17',0),(20,18,'修改权限','/admin/sysfunctioin/updatefunction',2,'2015-03-17 05:47:21',0),(21,18,'添加权限','/admin/sysfunctioin/addfunction',2,'2015-03-17 05:47:23',0),(22,18,'拖拽权限','/admin/sysfunctioin/updateparentid',2,'2015-03-17 05:48:44',0),(23,18,'删除权限','/admin/sysfunctioin/deletefunction',2,'2015-03-17 05:50:30',0),(24,0,'用户管理','',1,'2015-03-17 05:50:30',9),(25,24,'用户列表','/admin/sysuser/userlist',1,'2015-03-17 05:50:30',0),(26,19,'保存角色权限','/admin/sysrole/saveroelfunction/',2,'2015-03-19 02:56:09',0),(31,24,'添加用户','/admin/sysuser/createuser',2,'2015-03-22 09:46:17',0),(33,24,'修改用户密码','/admin/sysuser/updatepwd/',2,'2015-03-22 09:48:55',0),(34,24,'修改用户','/admin/sysuser/updateuser',2,'2015-03-22 09:48:55',0),(35,24,'禁用或启用用户','/admin/sysuser/disableOrstart/',2,'2015-03-22 09:50:14',0),(45,0,'系统管理','',1,'2015-03-23 03:47:53',10),(49,82,'基本配置','',1,'2015-03-23 08:08:44',0),(140,49,'基本配置','/admin/websiteProfile/find/web',1,'2016-11-11 23:57:10',0);
/*!40000 ALTER TABLE `sys_function` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_login_log`
--

DROP TABLE IF EXISTS `sys_login_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_login_log` (
  `LOG_ID` int(11) NOT NULL AUTO_INCREMENT,
  `LOGIN_TIME` timestamp NULL DEFAULT NULL COMMENT '登录时间',
  `IP` varchar(20) DEFAULT NULL COMMENT '登录IP',
  `USER_ID` int(11) DEFAULT '0' COMMENT '用户ID',
  `OS_NAME` varchar(50) DEFAULT NULL COMMENT '操作系统',
  `USER_AGENT` varchar(50) DEFAULT NULL COMMENT '浏览器',
  PRIMARY KEY (`LOG_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=1241 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_login_log`
--

LOCK TABLES `sys_login_log` WRITE;
/*!40000 ALTER TABLE `sys_login_log` DISABLE KEYS */;
INSERT INTO `sys_login_log` VALUES (1195,'2016-11-11 12:45:10','192.168.40.1',1,'windows','chrome'),(1196,'2016-11-11 12:48:20','192.168.40.1',1,'windows','chrome'),(1197,'2016-11-11 12:53:44','192.168.40.1',1,'windows','chrome'),(1198,'2016-11-11 13:00:35','192.168.40.1',1,'windows','chrome'),(1199,'2016-11-11 13:08:30','192.168.40.1',1,'windows','chrome'),(1200,'2016-11-11 13:13:19','192.168.40.1',1,'windows','chrome'),(1201,'2016-11-11 13:21:05','192.168.40.1',1,'windows','chrome'),(1202,'2016-11-11 13:26:12','192.168.40.1',1,'windows','chrome'),(1203,'2016-11-11 14:05:06','192.168.40.1',1,'windows','chrome'),(1204,'2016-11-11 14:15:12','192.168.40.1',1,'windows','chrome'),(1205,'2016-11-11 15:02:17','192.168.40.1',1,'windows','chrome'),(1206,'2016-11-11 15:10:31','192.168.40.1',1,'windows','chrome'),(1207,'2016-11-11 15:13:45','192.168.40.1',1,'windows','chrome'),(1208,'2016-11-11 15:26:41','192.168.40.1',1,'windows','chrome'),(1209,'2016-11-11 15:35:27','192.168.40.1',1,'windows','chrome'),(1210,'2016-11-11 15:37:34','192.168.40.1',1,'windows','chrome'),(1211,'2016-11-11 15:45:46','192.168.40.1',1,'windows','chrome'),(1212,'2016-11-11 15:55:03','192.168.40.1',1,'windows','chrome'),(1213,'2016-11-11 15:57:55','192.168.40.1',1,'windows','chrome'),(1214,'2016-11-11 16:00:40','192.168.40.1',1,'windows','chrome'),(1215,'2016-11-11 16:07:14','192.168.40.1',1,'windows','chrome'),(1216,'2016-11-11 16:14:50','192.168.40.1',1,'windows','chrome'),(1217,'2016-11-11 16:18:25','192.168.40.1',1,'windows','chrome'),(1218,'2016-11-11 16:23:24','192.168.40.1',1,'windows','chrome'),(1219,'2016-11-11 16:49:09','192.168.40.1',1,'windows','chrome'),(1220,'2016-11-11 16:52:09','192.168.40.1',1,'windows','chrome'),(1221,'2016-11-11 16:52:53','192.168.40.1',1,'windows','chrome'),(1222,'2016-11-11 22:29:09','192.168.40.1',1,'windows','chrome'),(1223,'2016-11-11 22:32:30','192.168.40.1',1,'windows','chrome'),(1224,'2016-11-11 22:34:09','192.168.40.1',1,'windows','chrome'),(1225,'2016-11-11 22:44:21','192.168.40.1',1,'windows','chrome'),(1226,'2016-11-11 22:51:52','192.168.40.1',1,'windows','chrome'),(1227,'2016-11-11 22:57:12','192.168.40.1',1,'windows','chrome'),(1228,'2016-11-11 23:01:55','192.168.40.1',1,'windows','chrome'),(1229,'2016-11-11 23:06:13','192.168.40.1',1,'windows','chrome'),(1230,'2016-11-11 23:10:11','192.168.40.1',1,'windows','chrome'),(1231,'2016-11-11 23:15:14','192.168.40.1',1,'windows','chrome'),(1232,'2016-11-11 23:28:12','192.168.40.1',1,'windows','chrome'),(1233,'2016-11-11 23:37:32','192.168.40.1',1,'windows','chrome'),(1234,'2016-11-11 23:40:04','192.168.40.1',1,'windows','chrome'),(1235,'2016-11-11 23:41:32','192.168.40.1',1,'windows','chrome'),(1236,'2016-11-11 23:51:09','192.168.40.1',1,'windows','chrome'),(1237,'2016-11-11 23:55:43','192.168.40.1',1,'windows','chrome'),(1238,'2016-11-11 23:59:03','192.168.40.1',1,'windows','chrome'),(1239,'2016-11-12 00:08:47','192.168.40.1',1,'windows','chrome'),(1240,'2016-11-12 00:59:39','192.168.40.1',1,'windows','chrome');
/*!40000 ALTER TABLE `sys_login_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_role`
--

DROP TABLE IF EXISTS `sys_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_role` (
  `ROLE_ID` int(11) NOT NULL AUTO_INCREMENT COMMENT '角色ID',
  `ROLE_NAME` varchar(100) DEFAULT NULL COMMENT '角色名',
  `CREATE_TIME` timestamp NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`ROLE_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COMMENT='角色表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_role`
--

LOCK TABLES `sys_role` WRITE;
/*!40000 ALTER TABLE `sys_role` DISABLE KEYS */;
INSERT INTO `sys_role` VALUES (1,'系统管理员','2015-03-17 16:00:00'),(2,'商家','2015-03-18 01:53:32'),(3,'用户','2015-03-18 02:13:16'),(4,'销售','2016-01-13 19:09:05');
/*!40000 ALTER TABLE `sys_role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_role_function`
--

DROP TABLE IF EXISTS `sys_role_function`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_role_function` (
  `ROLE_ID` int(11) DEFAULT '0' COMMENT '角色ID',
  `FUNCTION_ID` int(11) DEFAULT '0' COMMENT '权限ID'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='角色权限关联表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_role_function`
--

LOCK TABLES `sys_role_function` WRITE;
/*!40000 ALTER TABLE `sys_role_function` DISABLE KEYS */;
INSERT INTO `sys_role_function` VALUES (8,17),(8,18),(8,20),(8,21),(3,45),(3,18),(3,21),(3,36),(3,38),(3,73),(3,76),(3,77),(3,74),(3,75),(3,39),(3,40),(3,46),(3,47),(3,59),(3,60),(3,50),(3,56),(3,57),(3,58),(2,61),(2,62),(2,80),(2,81),(4,24),(4,25),(4,31),(4,33),(4,34),(4,35),(4,36),(4,37),(4,69),(4,70),(4,71),(4,72),(4,38),(4,73),(4,76),(4,77),(4,78),(4,74),(4,75),(4,39),(4,40),(4,46),(4,47),(4,59),(4,60),(4,50),(4,55),(4,56),(4,57),(4,58),(4,61),(4,62),(4,80),(4,81),(4,107),(4,30),(4,32),(4,79),(4,42),(4,43),(4,83),(4,84),(4,44),(4,82),(4,49),(4,51),(4,68),(4,100),(4,91),(4,92),(4,93),(4,94),(4,95),(4,96),(4,99),(4,97),(4,98),(4,108),(4,109),(4,110),(4,111),(4,112),(4,113),(4,114),(4,115),(4,116),(4,117),(4,118),(4,126),(4,129),(4,130),(4,131),(4,132),(4,133),(4,134),(4,135),(4,136),(1,45),(1,18),(1,20),(1,21),(1,22),(1,23),(1,19),(1,26),(1,24),(1,25),(1,31),(1,33),(1,34),(1,35),(1,49),(1,140);
/*!40000 ALTER TABLE `sys_role_function` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_subject`
--

DROP TABLE IF EXISTS `sys_subject`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_subject` (
  `SUBJECT_ID` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `SUBJECT_NAME` varchar(50) NOT NULL DEFAULT '' COMMENT '专业名称',
  `STATUS` tinyint(1) NOT NULL DEFAULT '0' COMMENT '状态 0:默认 1:删除',
  `CREATE_TIME` datetime DEFAULT NULL COMMENT '创建时间',
  `PARENT_ID` int(11) DEFAULT '0' COMMENT '父ID',
  `sort` int(11) DEFAULT '0' COMMENT '排序字段',
  PRIMARY KEY (`SUBJECT_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=252 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='专业信息';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_subject`
--

LOCK TABLES `sys_subject` WRITE;
/*!40000 ALTER TABLE `sys_subject` DISABLE KEYS */;
INSERT INTO `sys_subject` VALUES (202,'外语考试',1,'2014-03-04 09:53:03',0,2),(203,'职称英语',1,'2014-03-04 09:53:24',202,6),(204,'英语四级',1,'2014-03-04 09:53:38',202,3),(206,'英语六级',1,'2014-03-04 09:54:10',202,7),(208,'教师资格证',1,'2014-06-15 23:33:33',0,0),(209,'教师',1,'2014-06-16 14:00:10',208,0),(210,'公务员',1,'2014-06-26 09:37:33',0,0),(211,'行测',1,'2014-06-26 09:37:59',210,0),(213,'面试',1,'2014-06-26 09:38:21',210,0),(214,'其他',1,'2014-06-26 09:38:29',210,0),(215,'幼儿',1,'2014-06-26 09:39:36',209,0),(216,'小学',1,'2014-06-26 09:39:47',208,0),(217,'初中',1,'2014-06-26 09:39:55',208,5),(218,'高中',1,'2014-06-26 09:40:05',208,6),(221,'移动开发',0,'2014-06-26 09:41:21',0,0),(222,'游戏开发',0,'2014-06-26 09:41:32',221,0),(223,'数据库',0,'2014-06-26 09:41:41',0,3),(224,'操作系统',0,'2014-06-26 09:41:51',0,0),(247,'新建专业',1,'2015-09-10 10:32:19',224,0),(248,'新建专业',1,'2015-09-10 10:34:50',247,0),(249,'新建专业',1,'2015-09-10 10:34:56',247,0),(250,'window',0,'2015-09-10 10:35:07',224,0),(251,'mysql',0,'2015-09-10 10:35:56',223,0);
/*!40000 ALTER TABLE `sys_subject` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_user`
--

DROP TABLE IF EXISTS `sys_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_user` (
  `USER_ID` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `LOGIN_NAME` varchar(20) NOT NULL DEFAULT '' COMMENT '登录名',
  `LOGIN_PWD` varchar(32) NOT NULL DEFAULT '' COMMENT '登录密码',
  `USER_NAME` varchar(50) DEFAULT NULL COMMENT '用户真实姓名名',
  `STATUS` tinyint(1) DEFAULT '0' COMMENT '状态.0: 正常,1:冻结,2：删除',
  `LAST_LOGIN_TIME` timestamp NULL DEFAULT NULL COMMENT '最后登录时间',
  `LAST_LOGIN_IP` varchar(20) DEFAULT NULL COMMENT '最后登录IP',
  `CREATE_TIME` timestamp NULL DEFAULT NULL COMMENT '创建时间',
  `EMAIL` varchar(50) DEFAULT NULL COMMENT '邮件地址',
  `TEL` varchar(12) DEFAULT NULL COMMENT '手机号码',
  `ROLE_ID` int(11) DEFAULT '0' COMMENT '所属角色ID',
  PRIMARY KEY (`USER_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='系统用户表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_user`
--

LOCK TABLES `sys_user` WRITE;
/*!40000 ALTER TABLE `sys_user` DISABLE KEYS */;
INSERT INTO `sys_user` VALUES (1,'admin','96e79218965eb72c92a549dd5a330112','小程序联盟',0,'2016-11-12 00:59:39','192.168.40.1','2015-03-16 09:45:46','test@52xiaochengxu.net.com','13051245785',1);
/*!40000 ALTER TABLE `sys_user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-11-12  9:03:24
